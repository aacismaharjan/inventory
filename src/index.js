import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import App from './App'
import { InventoryProvider } from './context'

ReactDOM.render(
  <BrowserRouter>
    <InventoryProvider>
      <App />
    </InventoryProvider>
  </BrowserRouter>,
  document.getElementById('root')
)
