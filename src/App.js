import React from 'react'
import Category from './components/Category'
import Unit from './components/Unit'
import InventoryItem from './components/items/InventoryItem'
import Supply from './components/Supply'
import { Route, Switch } from 'react-router-dom'
import Navigation from './components/Navigation'
import Copyright from './components/Copyright'
import Dashboard from './components/Dashboard'

export default function App() {
  return (
    <React.Fragment>
      <Navigation />
      <div className='container py-3'>
        <Switch>
          <Route exact path='/'>
            <Dashboard />
          </Route>
          <Route exact path='/category'>
            <Category />
          </Route>
          <Route exact path='/unit'>
            <Unit />
          </Route>
          <Route exact path='/stock'>
            <InventoryItem />
          </Route>
          <Route exact path='/supply'>
            <Supply />
          </Route>
          <Route exact path='/report'>
            <InventoryItem />
          </Route>
          <Route path='*'>
            <InventoryItem />
          </Route>
        </Switch>
        <Copyright />
      </div>
    </React.Fragment>
  )
}
