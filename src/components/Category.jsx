import React from 'react'
import { useState, useContext } from 'react'
import { InventoryContext } from '../context'

export default function Category() {
  const [name, setName] = useState('')
  const { categories, setCategories, stockItems } = useContext(InventoryContext)

  const handleClick = () => {
    if (!name) return
    const id = Math.floor(Math.random() * 100 + 1)
    setCategories([...categories, { id, name }])
    setName('')
  }

  return (
    <div className='card'>
      <div className='card-body'>
        <div className='input-group'>
          <input
            type='text'
            onChange={(e) => setName(e.target.value)}
            className='form-control'
            placeholder='Category Name'
            value={name}
          />
          <div className='input-group-append'>
            <button
              className='btn btn-outline-secondary'
              type='button'
              onClick={handleClick}
            >
              Add Category
            </button>
          </div>
        </div>

        <table className='table table-bordered m-0 mt-3'>
          <thead>
            <tr>
              <th scope='col'>#</th>
              <th scope='col'>Category Name</th>
            </tr>
          </thead>
          <tbody>
            {categories.length > 0 &&
              categories.map(({ name }, index) => (
                <tr key={index}>
                  <th scope='row'>{index + 1}</th>
                  <td>{name}</td>
                </tr>
              ))}
          </tbody>
        </table>

        <table className='table table-bordered m-0 mt-4'>
          <thead>
            <tr>
              <th scope='col'>#</th>
              <th scope='col'>Category Name</th>
              <th scope='col'>Items</th>
            </tr>
          </thead>
          <tbody>
            {stockItems.length > 0 &&
              stockItems.map(({ category, itemName }, index) => (
                <tr key={index}>
                  <th scope='row'>{index + 1}</th>
                  <td>{category}</td>
                  <td>{itemName}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    </div>
  )
}
