import React from 'react'

export default function Copyright() {
  return (
    <div className='py-5'>
      <h6 className='m-0 text-secondary text-center'>
        Made with{' '}
        <span role='img' aria-label='heart'>
          🖤
        </span>{' '}
        by UptechSys
      </h6>
    </div>
  )
}
