import React from 'react'
import { useContext } from 'react'
import { useState } from 'react'
import { InventoryContext } from '../../context'

export default function InventoryAddItem() {
  const {
    stockItems: itemsData,
    setStockItems: setItemsData,
    setFiltered,
  } = useContext(InventoryContext)
  const { categories, units } = useContext(InventoryContext)

  const [item, setItem] = useState({
    id: 1,
    category: 'Electronics &amp; Devices',
    unit: 'kg',
    panNum: null,
    date: '2020-14-10',
    itemName: null,
    quantity: null,
    categoryNum: null,
    type: null,
  })

  const handleChange = (event) => {
    const target = event.target
    const value = target.value
    const name = target.name

    setItem({
      ...item,
      [name]: value,
    })
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if (Object.values(item).some((value) => value === null)) {
      alert('Something went wrong!')
    } else {
      setItemsData([
        ...itemsData,
        { ...item, id: Math.floor(Math.random() * 100 + 1) },
      ])
    }
  }

  const handleSearchChange = (e) => {
    let temp = []
    let search_query = e.target.value.toUpperCase()
    for (let i = 0; i < itemsData.length; i++) {
      if (itemsData[i].itemName.toUpperCase().indexOf(search_query) > -1) {
        temp = [...temp, itemsData[i]]
        setFiltered(temp)
      }
    }
  }

  const handleDateChange = (e) => {
    let temp = []
    let search_query = e.target.value.toUpperCase()
    for (let i = 0; i < itemsData.length; i++) {
      if (itemsData[i].date.toUpperCase().indexOf(search_query) > -1) {
        temp = [...temp, itemsData[i]]
        setFiltered(temp)
      }
    }
  }

  return (
    <div>
      <div className='item-add py-4 text-center d-flex justify-content-between'>
        <div className='custom-control-inline'>
          <div className='form-group m-0 mr-3'>
            <input
              type='email'
              className='form-control'
              id='inputEmail4'
              placeholder='Search items'
              onChange={handleSearchChange}
            />
          </div>

          <div className='form-group m-0'>
            <input
              type='date'
              className='form-control'
              id='inputEmail4'
              onChange={handleDateChange}
            />
          </div>
        </div>
        <button
          className='btn btn-primary'
          data-toggle='modal'
          data-target='#item-add'
        >
          Add Item
        </button>
      </div>

      <div
        className='modal fade'
        id='item-add'
        tabIndex='-1'
        aria-labelledby='exampleModalLabel'
        aria-hidden='true'
      >
        <div className='modal-dialog'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h5 className='modal-title' id='exampleModalLabel'>
                Add Stock Details
              </h5>
              <button
                type='button'
                className='close'
                data-dismiss='modal'
                aria-label='Close'
              >
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>
            <div className='modal-body'>
              <form>
                <div className='form-group'>
                  <label htmlFor='category'>Item Category</label>
                  <select
                    id='category'
                    name='category'
                    className='form-control'
                    onChange={handleChange}
                  >
                    {categories.length > 0 &&
                      categories.map((item, index) => (
                        <option value={item.name} key={index}>
                          {item.name}
                        </option>
                      ))}
                  </select>
                </div>
                <div className='form-group'>
                  <label htmlFor='unit'>Unit</label>
                  <select
                    id='unit'
                    name='unit'
                    className='form-control'
                    onChange={handleChange}
                  >
                    {units.length > 0 &&
                      units.map((item, index) => (
                        <option value={item.name} key={index}>
                          {item.name}
                        </option>
                      ))}
                  </select>
                </div>
                <div className='form-group'>
                  <label htmlFor='pan'>Pan Number</label>
                  <input
                    type='text'
                    name='panNum'
                    className='form-control'
                    id='pan'
                    placeholder='ABCDEF1234'
                    onChange={handleChange}
                  />
                </div>
                <div className='form-group'>
                  <label htmlFor='date'>Date</label>
                  <input
                    type='date'
                    name='date'
                    className='form-control'
                    id='date'
                    onChange={handleChange}
                  />
                </div>
                <div className='form-group'>
                  <label htmlFor='item-name'>Item Name</label>
                  <input
                    type='text'
                    name='itemName'
                    className='form-control'
                    id='item-name'
                    required
                    placeholder='Iphone 12 Pro Max'
                    onChange={handleChange}
                  />
                </div>
                <div className='form-group'>
                  <label htmlFor='quantity'>Quantity</label>
                  <input
                    type='number'
                    name='quantity'
                    className='form-control'
                    id='quantity'
                    placeholder='30'
                    onChange={handleChange}
                  />
                </div>
                <div className='form-group'>
                  <label htmlFor='classification-number'>
                    Classification Number
                  </label>
                  <input
                    type='number'
                    className='form-control'
                    id='classification-number'
                    name='categoryNum'
                    placeholder='201'
                    onChange={handleChange}
                  />
                </div>

                <div className='custom-control custom-radio custom-control-inline'>
                  <input
                    type='radio'
                    id='consumable'
                    name='type'
                    className='custom-control-input'
                    onChange={handleChange}
                    value='consumable'
                  />
                  <label className='custom-control-label' htmlFor='consumable'>
                    Consumable
                  </label>
                </div>
                <div className='custom-control custom-radio custom-control-inline'>
                  <input
                    type='radio'
                    id='durable'
                    name='type'
                    className='custom-control-input'
                    onChange={handleChange}
                    value='durable'
                  />
                  <label className='custom-control-label' htmlFor='durable'>
                    Durable
                  </label>
                </div>
              </form>
            </div>
            <div className='modal-footer'>
              <button
                type='button'
                className='btn btn-secondary'
                data-dismiss='modal'
              >
                Close
              </button>
              <button
                type='button'
                className='btn btn-primary'
                data-dismiss='modal'
                onClick={handleSubmit}
              >
                Add Item
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
