import React from 'react'
import { useContext } from 'react'
import { InventoryContext } from '../../context'

export default function InventoryTable() {
  const {
    filtered,
    stockItems: itemsData,
    setStockItems: setItemsData,
  } = useContext(InventoryContext)
  return (
    <div className='table-responsive'>
      <table className='table table-bordered'>
        <thead>
          <tr>
            <th scope='col'>#</th>
            <th scope='col'>Category</th>
            <th scope='col'>Unit</th>
            <th scope='col'>Pan Number</th>
            <th scope='col'>Date</th>
            <th scope='col'>Item Name</th>
            <th scope='col'>Quantity</th>
            <th scope='col'>Classification</th>
            <th scope='col'>Item Type</th>
            <th scope='col'>Actions</th>
          </tr>
        </thead>
        <tbody>
          {filtered.length > 0 &&
            filtered.map(
              (
                {
                  id,
                  category,
                  unit,
                  panNum,
                  date,
                  itemName,
                  quantity,
                  categoryNum,
                  type,
                },
                index
              ) => (
                <tr key={index}>
                  <th scope='row'>{index}</th>
                  <td>{category}</td>
                  <td>{unit}</td>
                  <td>{panNum}</td>
                  <td>{date}</td>
                  <td>{itemName}</td>
                  <td>{quantity}</td>
                  <td>{categoryNum}</td>
                  <td>{type}</td>
                  <td>
                    {/* <button className='btn btn-success btn-sm mr-2'>Edit</button> */}
                    <button
                      className='btn btn-danger btn-sm'
                      onClick={() =>
                        setItemsData([
                          ...itemsData.filter((item) => item.id !== id),
                        ])
                      }
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              )
            )}
        </tbody>
      </table>
    </div>
  )
}
