import React from 'react'
import InventoryAddItem from './InventoryAddItem'
import InventoryTable from './InventoryTable'

export default function InventoryItem() {
  return (
    <div>
      <InventoryAddItem />
      <InventoryTable />
    </div>
  )
}
