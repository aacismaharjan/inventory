import React from 'react'
import { useContext, useState } from 'react'
import { InventoryContext } from '../context'

export function SupplyTable() {
  const { supplyItems, setSupplyItems } = useContext(InventoryContext)
  return (
    <table className='table table-bordered'>
      <thead>
        <tr>
          <th scope='col'>#</th>
          <th scope='col'>Date</th>
          <th scope='col'>Item Name</th>
          <th scope='col'>Quantity</th>
          <th scope='col'>Unit</th>
          <th scope='col'>Department</th>
          <th scope='col'>Actions</th>
        </tr>
      </thead>
      <tbody>
        {supplyItems.length > 0 &&
          supplyItems.map(
            ({ id, date, item, quantity, unit, department }, index) => (
              <tr key={index}>
                <th scope='row'>{id}</th>
                <td>{date}</td>
                <td>{item}</td>
                <td>{quantity}</td>
                <td>{unit}</td>
                <td>{department}</td>
                <td>
                  {/* <button className='btn btn-success btn-sm mr-2'>Edit</button> */}
                  <button
                    className='btn btn-danger btn-sm'
                    onClick={() =>
                      setSupplyItems([
                        ...supplyItems.filter((item) => item.id !== id),
                      ])
                    }
                  >
                    Delete
                  </button>
                </td>
              </tr>
            )
          )}
      </tbody>
    </table>
  )
}

function SupplyAdd() {
  let { supplyItems, setSupplyItems, items } = useContext(InventoryContext)
  const [item, setItem] = useState({
    id: 1,
    supplier: 'A & B Company',
    department: '123 Department',
    panNum: 101,
    item: 'cycle',
    amount: 2000,
    unit: 'Rs',
    quantity: 10,
    date: '2020-10-21',
    receivedBy: 'Aashish',
    condition: 'durable',
    description: "Hello dummy data, I'm here to save you",
    letter_num: 323232,
  })

  const { units } = useContext(InventoryContext)

  const handleChange = (event) => {
    const target = event.target
    const value = target.value
    const name = target.name

    setItem({
      ...item,
      [name]: value,
    })
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if (Object.values(item).some((value) => value === null)) {
      alert('Something went wrong!')
    } else {
      setSupplyItems([
        ...supplyItems,
        { ...item, id: Math.floor(Math.random() * 100 + 1) },
      ])
    }
  }

  return (
    <div>
      <div className='item-add py-4 text-center d-flex justify-content-between'>
        <button
          className='btn btn-primary ml-auto'
          data-toggle='modal'
          data-target='#suppy-item-add'
        >
          Add Supply
        </button>
      </div>

      <div
        className='modal fade'
        id='suppy-item-add'
        tabIndex='-1'
        aria-labelledby='exampleModalLabel'
        aria-hidden='true'
      >
        <div className='modal-dialog'>
          <div className='modal-content'>
            <div className='modal-header'>
              <h5 className='modal-title' id='exampleModalLabel'>
                New message
              </h5>
              <button
                type='button'
                className='close'
                data-dismiss='modal'
                aria-label='Close'
              >
                <span aria-hidden='true'>&times;</span>
              </button>
            </div>
            <div className='modal-body'>
              <form>
                <div className='form-group'>
                  <label htmlFor='supplier'>Supplier</label>
                  <input
                    type='text'
                    name='supplier'
                    className='form-control'
                    id='supplier'
                    required
                    onChange={handleChange}
                  />
                </div>
                <div className='form-group'>
                  <label htmlFor='department'>Department</label>
                  <select
                    id='department'
                    name='department'
                    className='form-control'
                    onChange={handleChange}
                  >
                    <option value='Department of A &amp; B'>
                      Department of A &amp; B
                    </option>
                    <option value='Central Department'>
                      Central Department
                    </option>
                  </select>
                </div>

                <div className='form-group'>
                  <label htmlFor='pan'>Pan Number</label>
                  <input
                    type='text'
                    name='panNum'
                    className='form-control'
                    id='pan'
                    onChange={handleChange}
                  />
                </div>

                <div className='form-group'>
                  <label htmlFor='item'>Item</label>
                  <select
                    id='item'
                    name='item'
                    className='form-control'
                    onChange={handleChange}
                  >
                    {items.length > 0 &&
                      items.map((item, index) => (
                        <option value={item} key={index}>
                          {item}
                        </option>
                      ))}
                  </select>
                </div>

                <div className='form-group'>
                  <label htmlFor='quantity'>Quantity</label>
                  <input
                    type='number'
                    name='quantity'
                    className='form-control'
                    id='quantity'
                    onChange={handleChange}
                  />
                </div>

                <div className='form-group'>
                  <label htmlFor='unit'>Unit</label>
                  <select
                    id='unit'
                    name='unit'
                    className='form-control'
                    onChange={handleChange}
                  >
                    {units.length > 0 &&
                      units.map((item, index) => (
                        <option value={item.name} key={index}>
                          {item.name}
                        </option>
                      ))}
                  </select>
                </div>

                <div className='form-group'>
                  <label htmlFor='item-amount'>Amount</label>
                  <input
                    type='number'
                    name='amount'
                    className='form-control'
                    id='item-amount'
                    required
                    onChange={handleChange}
                  />
                </div>

                <div className='form-group'>
                  <label htmlFor='date'>Date</label>
                  <input
                    type='date'
                    name='date'
                    className='form-control'
                    id='pan'
                    onChange={handleChange}
                  />
                </div>

                <div className='form-group'>
                  <label htmlFor='receivedBy'>Received By</label>
                  <input
                    type='text'
                    name='receivedBy'
                    className='form-control'
                    id='receivedBy'
                    required
                    onChange={handleChange}
                  />
                </div>

                <div className='form-group'>
                  <label htmlFor='condition'>Condition of Item</label>
                  <input
                    type='text'
                    className='form-control'
                    id='condition'
                    name='condition'
                    onChange={handleChange}
                  />
                </div>

                <div className='form-group'>
                  <label htmlFor='description'>Description</label>
                  <textarea
                    type='text'
                    className='form-control'
                    id='description'
                    name='description'
                    onChange={handleChange}
                  />
                </div>

                <div className='form-group'>
                  <label htmlFor='letter-number'>Letter Number</label>
                  <input
                    type='number'
                    className='form-control'
                    id='letter-number'
                    name='letter_num'
                    onChange={handleChange}
                  />
                </div>
              </form>
            </div>
            <div className='modal-footer'>
              <button
                type='button'
                className='btn btn-secondary'
                data-dismiss='modal'
              >
                Close
              </button>
              <button
                type='button'
                className='btn btn-primary'
                data-dismiss='modal'
                onClick={handleSubmit}
              >
                Add Suppy
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default function Supply() {
  return (
    <div className='py-5'>
      <SupplyAdd />
      <SupplyTable />
    </div>
  )
}
