import React from 'react'
import { Link } from 'react-router-dom'

export default function Navigation() {
  const navLinks = [
    {
      link: '/',
      title: 'Dashboard',
    },
    {
      link: '/category',
      title: 'Category',
    },
    {
      link: '/unit',
      title: 'Unit',
    },
    {
      link: '/stock',
      title: 'Stock',
    },
    {
      link: '/supply',
      title: 'Supply',
    },
  ]

  const NavLink = ({ item: { link, title } }) => {
    return (
      <li className='nav-item'>
        <Link className={`nav-link`} to={link}>
          {title}
        </Link>
      </li>
    )
  }

  return (
    <nav className='navbar navbar-expand-lg navbar-light bg-light'>
      <div className='container'>
        <Link className='navbar-brand' to='/'>
          UptechSys
        </Link>
        <button
          className='navbar-toggler'
          type='button'
          data-toggle='collapse'
          data-target='#navbarSupportedContent'
          aria-controls='navbarSupportedContent'
          aria-expanded='false'
          aria-label='Toggle navigation'
        >
          <span className='navbar-toggler-icon'></span>
        </button>

        <div className='collapse navbar-collapse' id='navbarSupportedContent'>
          <ul className='navbar-nav ml-auto'>
            {navLinks.length > 0 &&
              navLinks.map((item, index) => (
                <NavLink item={item} key={index} />
              ))}
          </ul>
        </div>
      </div>
    </nav>
  )
}
