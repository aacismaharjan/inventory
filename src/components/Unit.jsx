import React from 'react'
import { useState, useContext } from 'react'
import { InventoryContext } from '../context'

export default function Unit() {
  const [name, setName] = useState('')
  const { units, setUnits } = useContext(InventoryContext)

  const handleClick = () => {
    if (!name) return
    const id = Math.floor(Math.random() * 100 + 1)
    setUnits([...units, { id, name }])
    setName('')
  }

  return (
    <div className='card'>
      <div className='card-body'>
        <div className='input-group'>
          <input
            type='text'
            onChange={(e) => setName(e.target.value)}
            className='form-control'
            placeholder='Unit Name'
            value={name}
          />
          <div className='input-group-append'>
            <button className='btn btn-outline-secondary' onClick={handleClick}>
              Add Unit
            </button>
          </div>
        </div>

        <table className='table table-bordered mt-3'>
          <thead>
            <tr>
              <th scope='col'>#</th>
              <th scope='col'>Unit Name</th>
            </tr>
          </thead>
          <tbody>
            {units.length > 0 &&
              units.map(({ name }, index) => (
                <tr key={index}>
                  <th scope='row'>{index}</th>
                  <td>{name}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    </div>
  )
}
