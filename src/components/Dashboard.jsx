import React from 'react'
import InventoryTable from './items/InventoryTable'
import { SupplyTable } from './Supply'

export default function Dashboard() {
  return (
    <div>
      <h3 className='text-center m-0 py-5'>Stock Report</h3>
      <InventoryTable />
      <div className='my-5'></div>
      <h3 className='text-center m-0 py-5'>Supply Report</h3>
      <SupplyTable />
    </div>
  )
}
