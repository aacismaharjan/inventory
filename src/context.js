import React, { useEffect, useState } from 'react'
const InventoryContext = React.createContext()

const InventoryProvider = ({ children }) => {
  const [categories, setCategories] = useState([
    { id: 1, name: 'Electronics & Devices' },
    { id: 2, name: 'Household' },
    { id: 2, name: 'Education' },
    { id: 2, name: 'Health' },
    { id: 2, name: 'Entertainment' },
  ])
  const [items, setItems] = useState([])
  const [stockItems, setStockItems] = useState([
    {
      id: 1,
      category: 'Electronics & Devices',
      unit: 'unit',
      panNum: 101,
      date: '2020-10-14',
      itemName: 'Iphone 12',
      quantity: 1,
      categoryNum: 1,
      type: 'durable',
    },
    {
      id: 2,
      category: 'Household',
      unit: 'kg',
      panNum: 101,
      date: '2020-10-14',
      itemName: 'Meat',
      quantity: 3,
      categoryNum: 2,
      type: 'durable',
    },
    {
      id: 3,
      category: 'Household',
      unit: 'kg',
      panNum: 101,
      date: '2020-10-13',
      itemName: 'Rice',
      quantity: 5,
      categoryNum: 2,
      type: 'consumable',
    },
    {
      id: 4,
      category: 'Electronics & Devices',
      unit: 'unit',
      panNum: 101,
      date: '2020-10-15',
      itemName: 'Laptop',
      quantity: 1,
      categoryNum: 1,
      type: 'durable',
    },
    {
      id: 5,
      category: 'Household',
      unit: 'kg',
      panNum: 101,
      date: '2020-10-09',
      itemName: 'Vegetables',
      quantity: 3,
      categoryNum: 2,
      type: 'consumable',
    },
    {
      id: 6,
      category: 'Household',
      unit: 'kg',
      panNum: 101,
      date: '2020-10-17',
      itemName: 'Wheat',
      quantity: 5,
      categoryNum: 2,
      type: 'consumable',
    },
    {
      id: 7,
      category: 'Electronics & Devices',
      unit: 'unit',
      panNum: 101,
      date: '2020-10-08',
      itemName: 'Laptop',
      quantity: 1,
      categoryNum: 1,
      type: 'durable',
    },
    {
      id: 8,
      category: 'Foods',
      unit: 'kg',
      panNum: 333,
      date: '2020-10-07',
      itemName: 'Pizza',
      quantity: 1,
      categoryNum: 3,
      type: 'consumable',
    },
    {
      id: 9,
      category: 'Health',
      unit: 'gram',
      panNum: 101,
      date: '2020-10-06',
      itemName: 'Bandages',
      quantity: 5,
      categoryNum: 3,
      type: 'durable',
    },
  ])

  const [supplyItems, setSupplyItems] = useState([
    {
      id: 1,
      supplier: 'A & B Company',
      department: '123 Department',
      panNum: 101,
      item: 'cycle',
      amount: 2000,
      unit: 'Rs',
      quantity: 10,
      date: '2020-10-21',
      receivedBy: 'Aashish',
      condition: 'durable',
      description: "Hello dummy data, I'm here to save you",
      letter_num: 323232,
    },
    {
      id: 2,
      supplier: 'Apple Company',
      department: '499 Department',
      panNum: 222,
      item: 'Ipod',
      amount: 40000,
      unit: 'Rs',
      quantity: 1,
      date: '2020-10-22',
      receivedBy: 'Rakesh',
      condition: 'consumable',
      description: "Hello dummy data, I'm here to save you",
      letter_num: 333333,
    },
    {
      id: 3,
      supplier: 'ABC Company Limited',
      department: 'XYZ Department',
      panNum: 101,
      item: 'Bike',
      amount: 2000,
      unit: '$',
      quantity: 5000,
      date: '2020-11-21',
      receivedBy: 'Mantosh',
      condition: 'durable',
      description: "Hello dummy data, I'm here to save you",
      letter_num: 323232,
    },
    {
      id: 4,
      supplier: 'CG Group',
      department: '499 Department',
      panNum: 222,
      item: 'Wai Wai',
      amount: 40000,
      unit: 'Rs',
      quantity: 10,
      date: '2020-10-22',
      receivedBy: 'Rohan',
      condition: 'consumable',
      description: "Hello dummy data, I'm here to save you",
      letter_num: 333333,
    },
    {
      id: 5,
      supplier: 'A & B Company',
      department: '123 Department',
      panNum: 101,
      item: 'Motorcycle',
      amount: 2000,
      unit: 'Nrs',
      quantity: 11,
      date: '2020-12-21',
      receivedBy: 'Aashish',
      condition: 'durable',
      description: "Hello dummy data, I'm here to save you",
      letter_num: 323232,
    },
    {
      id: 6,
      supplier: 'Apple Company',
      department: '499 Department',
      panNum: 222,
      item: 'Ipod',
      amount: 40000,
      unit: 'Rs',
      quantity: 1,
      date: '2020-9-22',
      receivedBy: 'Rakesh',
      condition: 'consumable',
      description: "Hello dummy data, I'm here to save you",
      letter_num: 333333,
    },
  ])

  const [units, setUnits] = useState([
    { id: 1, name: 'kg' },
    { id: 2, name: 'meter' },
    { id: 3, name: 'litre' },
    { id: 3, name: 'gram' },
    { id: 3, name: 'second' },
  ])

  const [filtered, setFiltered] = useState(stockItems)

  useEffect(() => {
    setFiltered(stockItems)

    let tempItems = []
    stockItems.map((item) => tempItems.push(item.itemName))

    setItems(tempItems)
  }, [stockItems])

  return (
    <InventoryContext.Provider
      value={{
        categories,
        setCategories,
        units,
        setUnits,
        stockItems,
        setStockItems,
        filtered,
        setFiltered,
        supplyItems,
        setSupplyItems,
        items,
      }}
    >
      {children}
    </InventoryContext.Provider>
  )
}

export { InventoryProvider, InventoryContext }
